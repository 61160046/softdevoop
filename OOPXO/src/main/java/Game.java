
import java.util.Scanner;

public class Game {

    private Board board;
    private Player O;
    private Player X;
    private int Xway;
    private int Yway;

    Scanner AA = new Scanner(System.in);

    public Game() {
        O = new Player('O');
        X = new Player('X');
    }

    public void makeBoard() {
        board = new Board(O, X);
    }

    public void showWelcome() {
        System.out.print("-------------------------\n"
                + "   Welcome to XO Game   \n"
                + "-------------------------\n");
        System.out.println();
    }

    public void showBoard() {
        char data[][] = board.getData();
        System.out.println("  1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < 3; j++) {
                System.out.print(" " + data[i][j]);
            }
            System.out.print("\n");
        }
    }

    public void showTurn() {
        System.out.println(board.getPlayer().getName() + " turn ");
    }

    public boolean inputBoardsize() {
        System.out.print("Please Input Where You Want to Play : ");
        try {
            Xway = AA.nextInt();
            Yway = AA.nextInt();
            board.setXY(Xway, Yway);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean goNext() {
        System.out.print("Next (Y/N): ");
        String next = AA.next();
        if (next.equals("Y")) {
            return true;
        }
        return false;
    }

    public void showWin(Player player) {
        this.showBoard();
        if (board.getWin() == null) {
            System.out.println("Draw!!!");
        } else {
            System.out.println("Player " + player.getName() + " Win ");
        }
        System.out.println("O Count= Win : " + O.getWin() + " Lose : "
                + O.getLose() + " Draw : " + O.getDraw());
        System.out.println("X Count= Win : " + X.getWin() + " Lose : "
                + X.getLose() + " Draw : " + X.getDraw());
    }

    public void showEnd() {
        System.out.print("-------------------------\n"
                + "           END   \n"
                + "-------------------------\n");
    }

    public void Play() {
        while (true) {
            this.showBoard();
            showTurn();
            System.out.println();
            if (this.inputBoardsize()) {
                if (board.winGame()) {
                    Player player = board.getWin();
                    this.showWin(player);
                    return;
                } else if (board.drawGame()) {
                    Player player = board.getWin();
                    this.showWin(player);
                    return;
                }
            }
        }

    }

    public void Run() {
        this.showWelcome();
        do {
            this.makeBoard();
            this.Play();
        } while (this.goNext());
        showEnd();
    }

}
